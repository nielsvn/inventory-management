package nl.intergamma.inventorymanagement.reservation;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
class CreateReservationDto {

    private long articleId;
    private long branchId;
    private long userId;
    private int amount;
}
