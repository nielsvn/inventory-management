package nl.intergamma.inventorymanagement.reservation;

import lombok.RequiredArgsConstructor;
import nl.intergamma.inventorymanagement.article.ArticleEntity;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class ArticleReservationService {

    private final ArticleReservationRepository reservationRepository;

    public List<ArticleReservationEntity> findAll() {
        return reservationRepository.findAll();
    }

    public Optional<ArticleReservationEntity> findById(long reservationId) {
        return reservationRepository.findById(reservationId);
    }

    public ArticleReservationEntity createReservation(CreateReservationDto reservation) {

        Instant nowIn30Minutes = Instant.now().plus(30, ChronoUnit.MINUTES);
        ArticleReservationEntity entity = new ArticleReservationEntity(new ArticleEntity(reservation.getArticleId(), null, null), reservation.getBranchId(), reservation.getUserId(), reservation.getAmount(), nowIn30Minutes);
        return reservationRepository.save(entity);
    }

    public void deleteById(long reservationId) {
        reservationRepository.deleteById(reservationId);
    }
}
