package nl.intergamma.inventorymanagement.reservation;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@RestController
@RequestMapping("/articles/{articleId}/reservations")
public class ArticleReservationController {

    private final ArticleReservationService reservationService;

    @GetMapping
    public List<ArticleReservationEntity> getAllArticleReservations() {
        return reservationService.findAll();
    }

    @GetMapping("/{articleReservationId}")
    public ResponseEntity<ArticleReservationEntity> getArticleReservationById(@PathVariable long articleReservationId) {
        Optional<ArticleReservationEntity> result = reservationService.findById(articleReservationId);
        if (result.isPresent()) {
            return ResponseEntity.ok(result.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping
    public ArticleReservationEntity createArticleReservation(@RequestBody CreateReservationDto articleReservation) {
        return reservationService.createReservation(articleReservation);
    }

    @DeleteMapping("/{articleReservationId}")
    public void deleteArticleReservation(@PathVariable long articleReservationId) {
        reservationService.deleteById(articleReservationId);
    }
}
