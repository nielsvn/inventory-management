package nl.intergamma.inventorymanagement.reservation;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
interface ArticleReservationRepository extends JpaRepository<ArticleReservationEntity, Long> { }
