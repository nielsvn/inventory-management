package nl.intergamma.inventorymanagement.reservation;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import nl.intergamma.inventorymanagement.article.ArticleEntity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.time.Instant;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
class ArticleReservationEntity {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private ArticleEntity article;

    private long branchId;
    private long userId;
    private int amount;
    private Instant reservedUntil;

    public ArticleReservationEntity(ArticleEntity article, long branchId, long userId, int amount, Instant reservedUntil) {
        this.article = article;
        this.branchId = branchId;
        this.userId = userId;
        this.amount = amount;
        this.reservedUntil = reservedUntil;
    }
}
