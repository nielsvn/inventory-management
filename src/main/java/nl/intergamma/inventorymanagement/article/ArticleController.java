package nl.intergamma.inventorymanagement.article;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@RestController
@RequestMapping("/articles")
public class ArticleController {

    private final ArticleRepository articleRepository;

    @GetMapping
    public List<ArticleEntity> getAllArticles() {
        return articleRepository.findAll();
    }

    @GetMapping("/{articleId}")
    public ResponseEntity<ArticleEntity> getArticleById(@PathVariable long articleId) {
        Optional<ArticleEntity> result = articleRepository.findById(articleId);
        if (result.isPresent()) {
            return ResponseEntity.ok(result.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PreAuthorize("hasRole('WORKER')")
    @PostMapping
    public ArticleEntity createArticle(@RequestBody ArticleEntity article) {
        articleRepository.save(article);
        return article;
    }

    @PreAuthorize("hasRole('WORKER')")
    @PutMapping("/{articleId}")
    public ArticleEntity updateArticle(@RequestBody ArticleEntity article) {
        return articleRepository.save(article);
    }

    @PreAuthorize("hasRole('WORKER')")
    @DeleteMapping("/{articleId}")
    public void deleteArticle(@PathVariable long articleId) {
        articleRepository.deleteById(articleId);
    }
}
