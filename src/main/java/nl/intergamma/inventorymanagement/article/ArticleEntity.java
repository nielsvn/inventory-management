package nl.intergamma.inventorymanagement.article;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Table(uniqueConstraints={
        @UniqueConstraint(columnNames = {"productCode"})
})
public class ArticleEntity {

    @Id
    @GeneratedValue
    private Long id;

    private String name;
    private String productCode;

    public ArticleEntity(String name, String productCode) {
        this.name = name;
        this.productCode = productCode;
    }
}
