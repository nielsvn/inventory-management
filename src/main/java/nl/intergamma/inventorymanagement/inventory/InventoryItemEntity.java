package nl.intergamma.inventorymanagement.inventory;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import nl.intergamma.inventorymanagement.article.ArticleEntity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
class InventoryItemEntity {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private ArticleEntity article;

    private long branchId;
    private int amount;

    public InventoryItemEntity(ArticleEntity article, long branchId, int amount) {
        this.article = article;
        this.branchId = branchId;
        this.amount = amount;
    }
}
