package nl.intergamma.inventorymanagement.inventory;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
interface InventoryItemRepository extends JpaRepository<InventoryItemEntity, Long> {

    InventoryItemEntity findByBranchIdAndArticleId(long branchId, long articleId);
}
