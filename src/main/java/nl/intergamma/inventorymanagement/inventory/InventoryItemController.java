package nl.intergamma.inventorymanagement.inventory;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@RestController
@RequestMapping("/inventory")
@PreAuthorize("hasRole('WORKER')")
public class InventoryItemController {

    private final InventoryItemRepository inventoryService;

    @GetMapping
    public List<InventoryItemEntity> getAllInventoryItems() {
        return inventoryService.findAll();
    }

    @GetMapping("/{inventoryItemId}")
    public ResponseEntity<InventoryItemEntity> getInventoryItemById(@PathVariable long inventoryItemId) {
        Optional<InventoryItemEntity> result = inventoryService.findById(inventoryItemId);
        if (result.isPresent()) {
            return ResponseEntity.ok(result.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping
    public InventoryItemEntity createInventoryItem(@RequestBody InventoryItemEntity inventoryItem) {
        inventoryService.save(inventoryItem);
        return inventoryItem;
    }

    @PutMapping("/{inventoryItemId}")
    public InventoryItemEntity updateInventoryItem(@RequestBody InventoryItemEntity inventoryItem) {
        inventoryService.save(inventoryItem);
        return inventoryItem;
    }

    @DeleteMapping("/{inventoryItemId}")
    public void deleteInventoryItem(@PathVariable long inventoryItemId) {
        inventoryService.deleteById(inventoryItemId);
    }
}
