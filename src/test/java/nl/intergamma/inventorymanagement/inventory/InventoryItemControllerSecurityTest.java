package nl.intergamma.inventorymanagement.inventory;

import nl.intergamma.inventorymanagement.article.ArticleEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(InventoryItemController.class)
public class InventoryItemControllerSecurityTest {

    @MockBean
    private InventoryItemRepository inventoryItemRepository;

    @Autowired
    private MockMvc mvc;

    // Tests with no authentication
    @Test
    public void testGetInventoryItems_noAuthentication_shouldReturn401() throws Exception {

        mvc.perform(get("/inventory"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void testGetInventoryItemById_noAuthentication_shouldReturn401() throws Exception {

        mvc.perform(get("/inventory/1"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void testCreateInventoryItem_noAuthentication_shouldReturn401() throws Exception {

        mvc.perform(post("/inventory").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void testUpdateInventoryItemById_noAuthentication_shouldReturn401() throws Exception {

        mvc.perform(put("/inventory/1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void testDeleteInventoryItemById_noAuthentication_shouldReturn401() throws Exception {

        mvc.perform(delete("/inventory/1"))
                .andExpect(status().isUnauthorized());
    }

    // Tests as a worker
    @WithMockUser(roles = { "WORKER" })
    @Test
    public void testGetInventoryItems_asWorker_shouldReturn200() throws Exception {

        mvc.perform(get("/inventory"))
                .andExpect(status().isOk());
    }

    @WithMockUser(roles = { "WORKER" })
    @Test
    public void testGetInventoryItemById_asWorker_shouldReturn200() throws Exception {

        when(inventoryItemRepository.findById(1L)).thenReturn(Optional.of(new InventoryItemEntity(1L, new ArticleEntity(), 1, 20)));
        mvc.perform(get("/inventory/1"))
                .andExpect(status().isOk());
    }

    @WithMockUser(roles = { "WORKER" })
    @Test
    public void testCreateInventoryItem_asWorker_shouldReturn200() throws Exception {

        mvc.perform(post("/inventory").contentType(MediaType.APPLICATION_JSON).content("{}"))
                .andExpect(status().isOk());
    }

    @WithMockUser(roles = { "WORKER" })
    @Test
    public void testUpdateInventoryItemById_asWorker_shouldReturn200() throws Exception {

        mvc.perform(put("/inventory/1").contentType(MediaType.APPLICATION_JSON).content("{}"))
                .andExpect(status().isOk());
    }

    @WithMockUser(roles = { "WORKER" })
    @Test
    public void testDeleteInventoryItemById_asWorker_shouldReturn200() throws Exception {

        mvc.perform(delete("/inventory/1"))
                .andExpect(status().isOk());
    }

    // Tests as a user
    @WithMockUser
    @Test
    public void testGetInventoryItems_asUser_shouldReturn403() throws Exception {

        mvc.perform(get("/inventory"))
                .andExpect(status().isForbidden());
    }

    @WithMockUser
    @Test
    public void testGetInventoryItemById_asUser_shouldReturn403() throws Exception {

        mvc.perform(get("/inventory/1"))
                .andExpect(status().isForbidden());
    }

    @WithMockUser
    @Test
    public void testCreateInventoryItem_asUser_shouldReturn403() throws Exception {

        mvc.perform(post("/inventory").contentType(MediaType.APPLICATION_JSON).content("{}"))
                .andExpect(status().isForbidden());
    }

    @WithMockUser
    @Test
    public void testUpdateInventoryItemById_asUser_shouldReturn403() throws Exception {

        mvc.perform(put("/inventory/1").contentType(MediaType.APPLICATION_JSON).content("{}"))
                .andExpect(status().isForbidden());
    }

    @WithMockUser
    @Test
    public void testDeleteInventoryItemById_asUser_shouldReturn403() throws Exception {

        mvc.perform(delete("/inventory/1"))
                .andExpect(status().isForbidden());
    }
}
