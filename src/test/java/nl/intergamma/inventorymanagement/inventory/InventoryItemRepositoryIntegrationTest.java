package nl.intergamma.inventorymanagement.inventory;

import nl.intergamma.inventorymanagement.article.ArticleEntity;
import nl.intergamma.inventorymanagement.article.ArticleRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

@RunWith(SpringRunner.class)
@SpringBootTest
public class InventoryItemRepositoryIntegrationTest {

    @Autowired
    private ArticleRepository articleRepository;

    @Autowired
    private InventoryItemRepository inventoryItemRepository;

    private InventoryItemEntity storedEntityOne;
    private InventoryItemEntity storedEntityTwo;

    @Before
    public void setup() {
        ArticleEntity articleOne = new ArticleEntity("Article one", "P1");
        articleRepository.save(articleOne);

        ArticleEntity articleTwo = new ArticleEntity("Article two", "P2");
        articleRepository.save(articleTwo);

        InventoryItemEntity mockedEntityOne = new InventoryItemEntity(articleOne, 1, 50);
        storedEntityOne = inventoryItemRepository.save(mockedEntityOne);

        InventoryItemEntity mockedEntityTwo = new InventoryItemEntity(articleTwo, 2, 100);
        storedEntityTwo = inventoryItemRepository.save(mockedEntityTwo);
    }

    @After
    public void cleanUp() {
        inventoryItemRepository.deleteAll();
        articleRepository.deleteAll();
    }

    @Test
    public void testFindByBranchAndArticle() {

        InventoryItemEntity result = inventoryItemRepository.findByBranchIdAndArticleId(1, storedEntityOne.getArticle().getId());
        assertEquals(storedEntityOne, result);
    }

    @Test
    public void testFindAll() {

        List<InventoryItemEntity> all = inventoryItemRepository.findAll();
        assertEquals(2, all.size());
        assertEquals(storedEntityOne, all.get(0));
        assertEquals(storedEntityTwo, all.get(1));
    }

    @Test
    public void testFindById_success() {

        Optional<InventoryItemEntity> result = inventoryItemRepository.findById(storedEntityOne.getId());
        assertTrue(result.isPresent());
        assertEquals(storedEntityOne, result.get());
    }

    @Test
    public void testFindById_notFound() {

        Optional<InventoryItemEntity> result = inventoryItemRepository.findById(-1L);
        assertFalse(result.isPresent());
    }

    @Test
    public void testCreateInventoryItem() {

        ArticleEntity newArticle = new ArticleEntity("Article three", "P3");
        articleRepository.save(newArticle);

        InventoryItemEntity entity = new InventoryItemEntity(newArticle, 2, 500);

        InventoryItemEntity managedEntity = inventoryItemRepository.save(entity);
        assertNotNull(managedEntity.getId());

        Optional<InventoryItemEntity> storedEntity = inventoryItemRepository.findById(managedEntity.getId());
        assertTrue(storedEntity.isPresent());

        storedEntity.ifPresent(e -> {
            assertNotNull(e.getId());
            assertEquals(entity.getArticle(), e.getArticle());
            assertEquals(entity.getBranchId(), e.getBranchId());
            assertEquals(entity.getAmount(), e.getAmount());
        });
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void testCreateInventoryItem_unexistingArticle() {

        ArticleEntity newArticle = new ArticleEntity(-1L, "Article three", "P3");
        InventoryItemEntity entity = new InventoryItemEntity(newArticle, 2, 500);

        inventoryItemRepository.save(entity);
    }

    @Test
    public void testUpdateInventoryItem() {

        int newAmount = 9999;

        // Create a new entity to make sure we're not touching a managed entity
        InventoryItemEntity updateEntity = new InventoryItemEntity(storedEntityTwo.getId(), storedEntityTwo.getArticle(), storedEntityTwo.getBranchId(), newAmount);

        inventoryItemRepository.save(updateEntity);

        Optional<InventoryItemEntity> storedUpdate = inventoryItemRepository.findById(storedEntityTwo.getId());
        assertTrue(storedUpdate.isPresent());
        assertEquals(updateEntity, storedUpdate.get());
    }

    @Test
    public void testDeleteInventoryItem() {

        inventoryItemRepository.deleteById(storedEntityOne.getId());

        Optional<InventoryItemEntity> result = inventoryItemRepository.findById(storedEntityOne.getId());
        assertFalse(result.isPresent());
    }

    @Test(expected = EmptyResultDataAccessException.class)
    public void testDeleteInventoryItem_notExisting() {
        inventoryItemRepository.deleteById(-1L);
    }
}
