package nl.intergamma.inventorymanagement.inventory;

import nl.intergamma.inventorymanagement.article.ArticleEntity;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class InventoryItemControllerIntegrationTest {

    @Autowired
    private TestRestTemplate restTemplate;

    private ArticleEntity existingArticle;

    @Before
    public void setup() {
        ArticleEntity toCreate = new ArticleEntity("Temporary Article", "T2");
        ResponseEntity<ArticleEntity> responseEntity = restTemplate
                .withBasicAuth("worker", "password")
                .postForEntity("/articles", toCreate, ArticleEntity.class);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

        existingArticle = responseEntity.getBody();
    }

    @After
    public void tearDown() {
        restTemplate
                .withBasicAuth("worker", "password")
                .delete("/articles/{articleId}", existingArticle.getId());
    }

    @Test
    public void testFindNonExistingInventoryItem_returns404() {
        ResponseEntity<InventoryItemEntity> result = restTemplate
                .withBasicAuth("worker", "password")
                .getForEntity("/inventory/-1", InventoryItemEntity.class);
        assertEquals(HttpStatus.NOT_FOUND, result.getStatusCode());
    }

    @Test
    public void testCreateAndDeleteInventoryItem() {

        // Create article
        InventoryItemEntity toCreate = new InventoryItemEntity(existingArticle, 1, 300);
        ResponseEntity<InventoryItemEntity> responseEntity = restTemplate
                .withBasicAuth("worker", "password")
                .postForEntity("/inventory", toCreate, InventoryItemEntity.class);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

        // Assert the response contains the same article
        InventoryItemEntity responseArticle = responseEntity.getBody();
        assertNotNull(responseArticle.getId());
        assertEquals(toCreate.getArticle(), responseArticle.getArticle());
        assertEquals(toCreate.getBranchId(), responseArticle.getBranchId());
        assertEquals(toCreate.getAmount(), responseArticle.getAmount());

        // Assert that it's also stored properly
        ResponseEntity<InventoryItemEntity> storedArticle = restTemplate
                .withBasicAuth("worker", "password")
                .getForEntity("/inventory/{inventoryId}", InventoryItemEntity.class, responseArticle.getId());
        assertEquals(HttpStatus.OK, storedArticle.getStatusCode());
        assertEquals(responseEntity.getBody(), storedArticle.getBody());

        // Delete it
        restTemplate
                .withBasicAuth("worker", "password")
                .delete("/inventory/{inventoryId}", responseArticle.getId());

        // Assert that it's not visible anymore
        storedArticle = restTemplate
                .withBasicAuth("worker", "password")
                .getForEntity("/inventory/{inventoryId}", InventoryItemEntity.class, responseArticle.getId());
        assertEquals(HttpStatus.NOT_FOUND, storedArticle.getStatusCode());
    }

    @Test
    public void testDeleteNonExistingInventoryItem_returnsSuccessful() {
        restTemplate
                .withBasicAuth("user", "password")
                .delete("/inventory/-1");
    }
}
