package nl.intergamma.inventorymanagement.reservation;

import nl.intergamma.inventorymanagement.article.ArticleEntity;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class ArticleReservationControllerIntegrationTest {

    @Autowired
    private TestRestTemplate restTemplate;

    private ArticleEntity existingArticle;

    @Before
    public void setup() {
        ArticleEntity toCreate = new ArticleEntity("Temporary Article", "T1");
        ResponseEntity<ArticleEntity> responseEntity = restTemplate
                .withBasicAuth("worker", "password")
                .postForEntity("/articles", toCreate, ArticleEntity.class);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

        existingArticle = responseEntity.getBody();
    }

    @After
    public void tearDown() {
        restTemplate
                .withBasicAuth("worker", "password")
                .delete("/articles/{articleId}", existingArticle.getId());
    }

    @Test
    public void testFindNonExistingArticleReservation_returns404() {
        ResponseEntity<ArticleReservationEntity> result = restTemplate
                .withBasicAuth("worker", "password")
                .getForEntity("/articles/-1/reservations/-1", ArticleReservationEntity.class);
        assertEquals(HttpStatus.NOT_FOUND, result.getStatusCode());
    }

    @Test
    public void testFindAllArticleReservation_returns200() {

        // Get all reservations
        ResponseEntity<List<ArticleReservationEntity>> result = restTemplate
                .withBasicAuth("worker", "password")
                .exchange("/articles/{articleId}/reservations", HttpMethod.GET, null, new ParameterizedTypeReference<List<ArticleReservationEntity>>() {}, existingArticle.getId());
        assertEquals(HttpStatus.OK, result.getStatusCode());
    }

    @Test
    public void testCreateAndDeleteArticleReservation() {

        // Create articleReservation
        CreateReservationDto toCreate = new CreateReservationDto(existingArticle.getId(), 1, 2, 2);
        ResponseEntity<ArticleReservationEntity> responseEntity = restTemplate
                .withBasicAuth("worker", "password")
                .postForEntity("/articles/{articleId}/reservations", toCreate, ArticleReservationEntity.class, existingArticle.getId());
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

        // Assert the response contains the same articleReservation
        ArticleReservationEntity responseReservation = responseEntity.getBody();
        assertNotNull(responseReservation.getId());
        assertEquals(toCreate.getArticleId(), (long) responseReservation.getArticle().getId());
        assertEquals(toCreate.getBranchId(), responseReservation.getBranchId());
        assertEquals(toCreate.getAmount(), responseReservation.getAmount());

        Instant nowIn30Minutes = Instant.now().plus(30, ChronoUnit.MINUTES);
        assertEquals(nowIn30Minutes.truncatedTo(ChronoUnit.MINUTES), responseReservation.getReservedUntil().truncatedTo(ChronoUnit.MINUTES));

        // Assert that it's also stored properly
        ResponseEntity<ArticleReservationEntity> storedReservation = restTemplate
                .withBasicAuth("worker", "password")
                .getForEntity("/articles/{articleId}/reservations/{inventoryId}", ArticleReservationEntity.class, responseReservation.getId(), responseReservation.getId());
        assertEquals(HttpStatus.OK, storedReservation.getStatusCode());

        ArticleReservationEntity storedEntity = storedReservation.getBody();
        assertEquals(toCreate.getArticleId(), (long) storedEntity.getArticle().getId());
        assertEquals(toCreate.getBranchId(), storedEntity.getBranchId());
        assertEquals(toCreate.getAmount(), storedEntity.getAmount());

        // Delete it
        restTemplate
                .withBasicAuth("worker", "password")
                .delete("/articles/{articleId}/reservations/{inventoryId}", responseReservation.getId(), responseReservation.getId());

        // Assert that it's not visible anymore
        storedReservation = restTemplate
                .withBasicAuth("worker", "password")
                .getForEntity("/articles/{articleId}/reservations/{inventoryId}", ArticleReservationEntity.class, responseReservation.getId(), responseReservation.getId());
        assertEquals(HttpStatus.NOT_FOUND, storedReservation.getStatusCode());
    }

    @Test
    public void testDeleteNonExistingArticleReservation_returnsSuccessful() {
        restTemplate
                .withBasicAuth("user", "password")
                .delete("/articles/-1/reservations/-1");
    }
}
