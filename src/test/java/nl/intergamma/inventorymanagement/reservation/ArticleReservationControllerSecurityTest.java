package nl.intergamma.inventorymanagement.reservation;

import nl.intergamma.inventorymanagement.article.ArticleEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.Instant;
import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ArticleReservationController.class)
public class ArticleReservationControllerSecurityTest {

    @MockBean
    private ArticleReservationService articleReservationService;

    @Autowired
    private MockMvc mvc;

    // Tests with no authentication
    @Test
    public void testGetArticleReservationss_noAuthentication_shouldReturn401() throws Exception {

        mvc.perform(get("/articles/1/reservations"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void testGetArticleReservationsById_noAuthentication_shouldReturn401() throws Exception {

        mvc.perform(get("/articles/1/reservations/1"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void testCreateArticleReservations_noAuthentication_shouldReturn401() throws Exception {

        mvc.perform(post("/articles/1/reservations").contentType(MediaType.APPLICATION_JSON).content("{}"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void testDeleteArticleReservationsById_noAuthentication_shouldReturn401() throws Exception {

        mvc.perform(delete("/articles/1/reservations/1"))
                .andExpect(status().isUnauthorized());
    }

    // Tests as a worker
    @WithMockUser(roles = { "WORKER" })
    @Test
    public void testGetArticleReservationss_asWorker_shouldReturn200() throws Exception {

        mvc.perform(get("/articles/1/reservations"))
                .andExpect(status().isOk());
    }

    @WithMockUser(roles = { "WORKER" })
    @Test
    public void testGetArticleReservationsById_asWorker_shouldReturn200() throws Exception {

        when(articleReservationService.findById(1L)).thenReturn(Optional.of(new ArticleReservationEntity(1L, new ArticleEntity(), 1, 2, 3, Instant.now())));
        mvc.perform(get("/articles/1/reservations/1"))
                .andExpect(status().isOk());
    }

    @WithMockUser(roles = { "WORKER" })
    @Test
    public void testCreateArticleReservations_asWorker_shouldReturn200() throws Exception {

        mvc.perform(post("/articles/1/reservations").contentType(MediaType.APPLICATION_JSON).content("{}"))
                .andExpect(status().isOk());
    }

    @WithMockUser(roles = { "WORKER" })
    @Test
    public void testDeleteArticleReservationsById_asWorker_shouldReturn200() throws Exception {

        mvc.perform(delete("/articles/1/reservations/1"))
                .andExpect(status().isOk());
    }

    // Tests as a user
    @WithMockUser
    @Test
    public void testGetArticleReservationss_asUser_shouldReturn200() throws Exception {

        mvc.perform(get("/articles/1/reservations"))
                .andExpect(status().isOk());
    }

    @WithMockUser
    @Test
    public void testGetArticleReservationsById_asUser_shouldReturn200() throws Exception {

        when(articleReservationService.findById(1L)).thenReturn(Optional.of(new ArticleReservationEntity(1L, new ArticleEntity(), 1, 2, 3, Instant.now())));
        mvc.perform(get("/articles/1/reservations/1"))
                .andExpect(status().isOk());
    }

    @WithMockUser
    @Test
    public void testCreateArticleReservations_asUser_shouldReturn200() throws Exception {

        mvc.perform(post("/articles/1/reservations").contentType(MediaType.APPLICATION_JSON).content("{}"))
                .andExpect(status().isOk());
    }

    @WithMockUser
    @Test
    public void testDeleteArticleReservationsById_asUser_shouldReturn200() throws Exception {

        mvc.perform(delete("/articles/1/reservations/1"))
                .andExpect(status().isOk());
    }
}
