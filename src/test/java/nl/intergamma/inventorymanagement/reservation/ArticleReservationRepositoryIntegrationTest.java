package nl.intergamma.inventorymanagement.reservation;

import nl.intergamma.inventorymanagement.article.ArticleEntity;
import nl.intergamma.inventorymanagement.article.ArticleRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ArticleReservationRepositoryIntegrationTest {

    @Autowired
    private ArticleRepository articleRepository;

    @Autowired
    private ArticleReservationRepository articleReservationRepository;

    private ArticleReservationEntity storedEntityOne;
    private ArticleReservationEntity storedEntityTwo;

    @Before
    public void setup() {

        ArticleEntity articleOne = new ArticleEntity("Article one", "P1");
        articleRepository.save(articleOne);

        ArticleEntity articleTwo = new ArticleEntity("Article two", "P2");
        articleRepository.save(articleTwo);

        ArticleReservationEntity mockedEntityOne = new ArticleReservationEntity(articleOne, 1, 1, 2, nowPlus30Minutes());
        storedEntityOne = articleReservationRepository.save(mockedEntityOne);

        ArticleReservationEntity mockedEntityTwo = new ArticleReservationEntity(articleTwo, 1, 2, 1, nowPlus30Minutes());
        storedEntityTwo = articleReservationRepository.save(mockedEntityTwo);
    }

    @After
    public void cleanUp() {
        articleReservationRepository.deleteAll();
        articleRepository.deleteAll();
    }

    @Test
    public void testFindAll() {

        List<ArticleReservationEntity> all = articleReservationRepository.findAll();
        assertEquals(2, all.size());
        assertEquals(storedEntityOne, all.get(0));
        assertEquals(storedEntityTwo, all.get(1));
    }

    @Test
    public void testFindById_success() {

        Optional<ArticleReservationEntity> result = articleReservationRepository.findById(storedEntityOne.getId());
        assertTrue(result.isPresent());
        assertEquals(storedEntityOne, result.get());
    }

    @Test
    public void testFindById_notFound() {

        Optional<ArticleReservationEntity> result = articleReservationRepository.findById(-1L);
        assertFalse(result.isPresent());
    }

    @Test
    public void testCreateArticleReservation() {

        ArticleReservationEntity entity = new ArticleReservationEntity(storedEntityOne.getArticle(), 1, 3, 10, nowPlus30Minutes());

        ArticleReservationEntity managedEntity = articleReservationRepository.save(entity);
        assertNotNull(managedEntity.getId());

        Optional<ArticleReservationEntity> storedEntity = articleReservationRepository.findById(managedEntity.getId());
        assertTrue(storedEntity.isPresent());

        storedEntity.ifPresent(e -> {
            assertNotNull(e.getId());
            assertEquals(entity.getArticle(), e.getArticle());
            assertEquals(entity.getBranchId(), e.getBranchId());
            assertEquals(entity.getAmount(), e.getAmount());
        });
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void testCreateArticleReservation_unexistingArticle() {

        ArticleEntity newArticle = new ArticleEntity(-1L, "Article three", "P3");
        ArticleReservationEntity entity = new ArticleReservationEntity(newArticle, 1, 3, 10, nowPlus30Minutes());

        articleReservationRepository.save(entity);
    }

    @Test
    public void testUpdateArticleReservation() {

        int newAmount = 9999;

        // Create a new entity to make sure we're not touching a managed entity
        ArticleReservationEntity updateEntity = new ArticleReservationEntity(storedEntityTwo.getId(), storedEntityTwo.getArticle(), storedEntityTwo.getBranchId(), storedEntityTwo.getUserId(), newAmount, nowPlus30Minutes());

        articleReservationRepository.save(updateEntity);

        Optional<ArticleReservationEntity> storedUpdate = articleReservationRepository.findById(storedEntityTwo.getId());
        assertTrue(storedUpdate.isPresent());
        assertEquals(updateEntity, storedUpdate.get());
    }

    @Test
    public void testDeleteArticleReservation() {

        articleReservationRepository.deleteById(storedEntityOne.getId());

        Optional<ArticleReservationEntity> result = articleReservationRepository.findById(storedEntityOne.getId());
        assertFalse(result.isPresent());
    }

    @Test(expected = EmptyResultDataAccessException.class)
    public void testDeleteArticleReservation_notExisting() {
        articleReservationRepository.deleteById(-1L);
    }

    private Instant nowPlus30Minutes() {
        return Instant.now().plus(30, ChronoUnit.MINUTES);
    }
}
