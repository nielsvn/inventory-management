package nl.intergamma.inventorymanagement.article;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ArticleRepositoryIntegrationTest {

    @Autowired
    private ArticleRepository articleRepository;

    private ArticleEntity storedEntityOne;
    private ArticleEntity storedEntityTwo;

    @Before
    public void setup() {
        ArticleEntity mockedEntityOne = new ArticleEntity("Article 1", "P1");
        storedEntityOne = articleRepository.save(mockedEntityOne);

        ArticleEntity mockedEntityTwo = new ArticleEntity("Article 2", "P2");
        storedEntityTwo = articleRepository.save(mockedEntityTwo);
    }

    @After
    public void cleanUp() {
        articleRepository.deleteAll();
    }

    @Test
    public void testFindAll() {

        List<ArticleEntity> all = articleRepository.findAll();
        assertEquals(2, all.size());
        assertEquals(storedEntityOne, all.get(0));
        assertEquals(storedEntityTwo, all.get(1));
    }

    @Test
    public void testFindById_success() {

        Optional<ArticleEntity> result = articleRepository.findById(storedEntityOne.getId());
        assertTrue(result.isPresent());
        assertEquals(storedEntityOne, result.get());
    }

    @Test
    public void testFindById_notFound() {

        Optional<ArticleEntity> result = articleRepository.findById(-1L);
        assertFalse(result.isPresent());
    }

    @Test
    public void testCreateArticle() {

        ArticleEntity entity = new ArticleEntity("Article", "12345");

        ArticleEntity managedEntity = articleRepository.save(entity);
        assertNotNull(managedEntity.getId());

        Optional<ArticleEntity> storedEntity = articleRepository.findById(managedEntity.getId());
        assertTrue(storedEntity.isPresent());

        storedEntity.ifPresent(e -> {
            assertNotNull(e.getId());
            assertEquals(entity.getName(), e.getName());
            assertEquals(entity.getProductCode(), e.getProductCode());
        });
    }

    @Test
    public void testUpdateArticle() {

        String newProductCode = "NEWP2";
        // Create a new entity to make sure we're not touching a managed entity
        ArticleEntity updateEntity = new ArticleEntity(storedEntityTwo.getId(), storedEntityTwo.getName(), newProductCode);

        articleRepository.save(updateEntity);

        Optional<ArticleEntity> storedUpdate = articleRepository.findById(storedEntityTwo.getId());
        assertTrue(storedUpdate.isPresent());
        assertEquals(updateEntity, storedUpdate.get());
    }

    @Test
    public void testDeleteArticle() {

        articleRepository.deleteById(storedEntityOne.getId());

        Optional<ArticleEntity> result = articleRepository.findById(storedEntityOne.getId());
        assertFalse(result.isPresent());
    }

    @Test(expected = EmptyResultDataAccessException.class)
    public void testDeleteArticle_notExisting() {
        articleRepository.deleteById(-1L);
    }
}
