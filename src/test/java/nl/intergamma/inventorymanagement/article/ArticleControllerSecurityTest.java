package nl.intergamma.inventorymanagement.article;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ArticleController.class)
public class ArticleControllerSecurityTest {

    @MockBean
    private ArticleRepository articleRepository;

    @Autowired
    private MockMvc mvc;

    // Tests with no authentication
    @Test
    public void testGetArticles_noAuthentication_shouldReturn401() throws Exception {

        mvc.perform(get("/articles"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void testGetArticleById_noAuthentication_shouldReturn401() throws Exception {

        mvc.perform(get("/articles/1"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void testCreateArticle_noAuthentication_shouldReturn401() throws Exception {

        mvc.perform(post("/articles").contentType(MediaType.APPLICATION_JSON).content("{}"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void testUpdateArticleById_noAuthentication_shouldReturn401() throws Exception {

        mvc.perform(put("/articles/1").contentType(MediaType.APPLICATION_JSON).content("{}"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void testDeleteArticleById_noAuthentication_shouldReturn401() throws Exception {

        mvc.perform(delete("/articles/1"))
                .andExpect(status().isUnauthorized());
    }

    // Tests as a worker
    @WithMockUser(roles = { "WORKER" })
    @Test
    public void testGetArticles_asWorker_shouldReturn200() throws Exception {

        mvc.perform(get("/articles"))
                .andExpect(status().isOk());
    }

    @WithMockUser(roles = { "WORKER" })
    @Test
    public void testGetArticleById_asWorker_shouldReturn200() throws Exception {

        when(articleRepository.findById(1L)).thenReturn(Optional.of(new ArticleEntity(1L, "Name", "P1")));
        mvc.perform(get("/articles/1"))
                .andExpect(status().isOk());
    }

    @WithMockUser(roles = { "WORKER" })
    @Test
    public void testCreateArticle_asWorker_shouldReturn200() throws Exception {

        mvc.perform(post("/articles").contentType(MediaType.APPLICATION_JSON).content("{}"))
                .andExpect(status().isOk());
    }

    @WithMockUser(roles = { "WORKER" })
    @Test
    public void testUpdateArticleById_asWorker_shouldReturn200() throws Exception {

        mvc.perform(put("/articles/1").contentType(MediaType.APPLICATION_JSON).content("{}"))
                .andExpect(status().isOk());
    }

    @WithMockUser(roles = { "WORKER" })
    @Test
    public void testDeleteArticleById_asWorker_shouldReturn200() throws Exception {

        mvc.perform(delete("/articles/1"))
                .andExpect(status().isOk());
    }

    // Tests as a user
    @WithMockUser
    @Test
    public void testGetArticles_asUser_shouldReturn200() throws Exception {

        mvc.perform(get("/articles"))
                .andExpect(status().isOk());
    }

    @WithMockUser
    @Test
    public void testGetArticleById_asUser_shouldReturn200() throws Exception {

        when(articleRepository.findById(1L)).thenReturn(Optional.of(new ArticleEntity(1L, "Name", "P1")));
        mvc.perform(get("/articles/1"))
                .andExpect(status().isOk());
    }

    @WithMockUser
    @Test
    public void testCreateArticle_asUser_shouldReturn403() throws Exception {

        mvc.perform(post("/articles").contentType(MediaType.APPLICATION_JSON).content("{}"))
                .andExpect(status().isForbidden());
    }

    @WithMockUser
    @Test
    public void testUpdateArticleById_asUser_shouldReturn403() throws Exception {

        mvc.perform(put("/articles/1").contentType(MediaType.APPLICATION_JSON).content("{}"))
                .andExpect(status().isForbidden());
    }

    @WithMockUser
    @Test
    public void testDeleteArticleById_asUser_shouldReturn403() throws Exception {

        mvc.perform(delete("/articles/1"))
                .andExpect(status().isForbidden());
    }
}
