package nl.intergamma.inventorymanagement.article;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class ArticleControllerIntegrationTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void testFindNonExistingArticle_returns404() {
        ResponseEntity<ArticleEntity> result = restTemplate
                .withBasicAuth("user", "password")
                .getForEntity("/articles/-1", ArticleEntity.class);
        assertEquals(HttpStatus.NOT_FOUND, result.getStatusCode());
    }

    @Test
    public void testCreateAndDeleteArticle() {

        // Create article
        ArticleEntity toCreate = new ArticleEntity("Article A", "A");
        ResponseEntity<ArticleEntity> responseEntity = restTemplate
                .withBasicAuth("worker", "password")
                .postForEntity("/articles", toCreate, ArticleEntity.class);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

        // Assert the response contains the same article
        ArticleEntity responseArticle = responseEntity.getBody();
        assertNotNull(responseArticle.getId());
        assertEquals(toCreate.getName(), responseArticle.getName());
        assertEquals(toCreate.getProductCode(), responseArticle.getProductCode());

        // Assert that it's also stored properly
        ResponseEntity<ArticleEntity> storedArticle = restTemplate
                .withBasicAuth("worker", "password")
                .getForEntity("/articles/{articleId}", ArticleEntity.class, responseArticle.getId());
        assertEquals(HttpStatus.OK, storedArticle.getStatusCode());
        assertEquals(responseEntity.getBody(), storedArticle.getBody());

        // Delete it
        restTemplate
                .withBasicAuth("worker", "password")
                .delete("/articles/{articleId}", responseArticle.getId());

        // Assert that it's not visible anymore
        storedArticle = restTemplate
                .withBasicAuth("worker", "password")
                .getForEntity("/articles/{articleId}", ArticleEntity.class, responseArticle.getId());
        assertEquals(HttpStatus.NOT_FOUND, storedArticle.getStatusCode());
    }

    @Test
    public void testDeleteNonExistingArticle_returnsSuccessful() {
        restTemplate
                .withBasicAuth("user", "password")
                .delete("/articles/-1");
    }
}
