# Inventory Management

This is a simplified inventory management system which can perform basic operations like manage the articles and their inventory.

## Run application
You can run the application by executing the following command in the project root:
`./gradlew bootRun`

This application can also be run without Gradle, by running the command `java -jar build/libs/inventory-management-0.0.1-SNAPSHOT.jar` in the project root.\
NOTE: this requires the latest version of the code being built with `./gradlew build`.

## REST API
The following REST endpoints are available:
- `/articles(/{articleId})`
- `/articles/{articleId}/reservation`
- `/inventory(/{inventoryId})`

All endpoints support CRUD with POST, GET, PUT and DELETE.

## Security
There are two roles defined:
- `USER`
    - An end-user which should be able to read articles and do reservations on articles.
- `WORKER`
    - A warehouse worker which should be able to manage the inventory and the articles.
    
No OAuth2 has been implemented
    
## Decisions
- Since the application is built using Spring Boot, I've chosen not to run it on Tomcat itself, since Spring Boot already has Tomcat 9 embedded in the application.
- I've chosen to only include a `service` layer between the controllers and repositories when:
    - There's business logic needed between the layers
    - There are other services calling this repository. This is to prevent big refactorings in the future.
- I've chosen to not use Spring Rest Repositories, because it mandates the use of HATEAOS and limits the decisions we can make later on. Currently, there are no requirements for HATEAOS. If this requirement comes, we can always revise this decision.
- The entities `Branch` and `User` haven't been implemented. These would exist in a real application of course. Instead, you'll only encounter their IDs.

## Assumptions
 - The application can only set the stock, increments/decrements should be calculated externally.